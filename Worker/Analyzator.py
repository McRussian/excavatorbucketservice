__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

from random import random
from Logger.DataFrameLog import DataFrameLog as Data
from Worker.ResultWork import ResultWork
from Worker.SharedBuffer import SharedBuffer


class BucketAnalyze:
    _probabilty: dict = None
    _name: str = 'BucketModel'
    _data_buffer: SharedBuffer = None

    def __init__(self, buffer: SharedBuffer):
        self._data_buffer = buffer
        self._probabilty = {
            'normal': (0.0015, 'white'),
            'hidden': (0.01, 'gray'),
            'broken': (0.6, 'red'),
        }

    def Analyze(self, data: ResultWork)-> str:
        classes = data.GetClasses()
        value = random()
        if value < self._probabilty[classes][0]:
            try:
                self._data_buffer.push(Data(frame=data.GetFrame(), namemodel=self._name, classes=data.GetClasses()))
            except:
                pass

        return self._probabilty[classes][1]

class TruckAnalyze:
    _probabilty: dict = None
    _name: str = 'TruckModel'
    _data_buffer: SharedBuffer = None

    def __init__(self, buffer: SharedBuffer):
        self._data_buffer = buffer
        self._probabilty = {
            'truck': (0.01, 'white'),
        }

    def Analyze(self, data: ResultWork):
        classes = data.GetClasses()
        value = random()
        if value < self._probabilty[classes][0]:
            try:
                self._data_buffer.push(Data(frame=data.GetFrame(), namemodel=self._name, classes=data.GetClasses()))
            except:
                pass
        return self._probabilty[classes][1]



Analyzator = {
    'Bucket Model': BucketAnalyze,
    'Truck Model': TruckAnalyze
}
