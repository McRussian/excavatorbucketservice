__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

import threading
import time
import random

class PassiveThread (threading.Thread):
    '''
    Объект этого класа представляет собой пассивный поток.
    Поток находится в состоянии ожидания, пока к нему не придут данные,
    после обработки этих данных, поток снова переходит в состояние ожидания.
    '''
    _mutex_work = None
    _mutex_data = None
    _is_shutdown = False
    _data = None


    def __init__(self):
        threading.Thread.__init__(self)
        self._mutex_work = threading.Lock()
        self._mutex_work.acquire()
        self._mutex_data = threading.Lock()

    def run(self):
        while True:
            self._mutex_work.acquire()
            if self._is_shutdown:
                self._mutex_work.release()
                break
            self.WorkData()

    def PushData(self, data):
        self._mutex_data.acquire()
        self._data = data
        self._mutex_work.release()
        self._mutex_data.release()


    def WorkData(self):
        self._mutex_data.acquire()
        print('Work thread')
        time.sleep(random.randint(2, 4))

        self._mutex_data.release()
        self._mutex_work.acquire()

    def __del__(self):
        print('Delete Passive Thread')


    def stop(self):
        while not self._mutex_work.locked():
            pass
        self._mutex_work.release()
        self._is_shutdown = True
