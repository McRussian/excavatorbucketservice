__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

from time import sleep
from Logger.Logger import Logger
from Worker import Worker
from Worker import WorkerException
from Setting import CommonSetting

from Worker import VideoStream
from Worker import SharedBuffer

from GUI.Controller import ControllerScreen

class Controller:
    '''
    Объект этого класса отвечает за организацию работы всех работников.
    Его задача
    1. Создать объект видеопотока, с которого будут браться кадры
    2. Создать список моделей, которые будут использоваться
    3. ДЛя каждой модели создать работника и запустить их.

    Предлагает интерфейс для управления их работой, типа старт, стоп, и т.п.
    '''

    _setting = None
    _logger = None
    _buffer = None
    _stream = None
    _workers = None
    _callback = None

    def __init__(self, setting:  CommonSetting.CommonSetting, logger: Logger):
        self._setting = setting
        self._logger = logger
        self._workers = list()
        self._buffer = SharedBuffer.SharedBuffer(5)
        self._screen: ControllerScreen = None

    def InitializationVideostream(self):
        try:
            self._stream = VideoStream.VideoStream(sett=self._setting.GetSettingVideoStream(), buffer=self._buffer, logger=self._logger)
        except WorkerException.VideoStreamException as err:
            self._logger.PrintLog(err.GetErrorCode(), err.GetErrorMessage())
            raise WorkerException.WorkerException(1, err.GetErrorMessage())

    def Initizialization(self, callback):
        self._logger.PrintLog(100, 'Initialization Controller')
        self.InitializationVideostream()

        self._screen = ControllerScreen(buffer=self._buffer, logger=self._logger, closecallback=self.Stop)
        self._callback = callback

    def Start(self, buffer: SharedBuffer):
        self._logger.PrintLog(110, 'Start Controller')
        self._stream.start()

        ls_setting = self._setting.GetListSettingModels().GetListSettingModels()
        result_buffers = {}
        for sett in ls_setting:
            worker = Worker.Worker(setting_model=sett, buffer=self._buffer,
                                   logger=self._logger, frame_buffer=buffer, callback=self.RestartVideoStream)
            worker.start()
            self._workers.append(worker)
            result_buffers[worker.GetResultBuffer().description()] = worker.GetResultBuffer()

        self._screen.SetResultsBuffer(result_buffers)
        self._screen.Start()

    def Stop(self):
        self._logger.PrintLog(111, 'Stop Controller')
        self._stream.Stop()
        self._callback()

        for worker in self._workers:
            worker.Stop()

    def RestartVideoStream(self):
        self._logger.PrintLog(11, 'Restart VideoStream')
        self._stream.Stop()
        sleep(5)
        self.InitializationVideostream()
        self._stream.start()

