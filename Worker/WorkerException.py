__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

import CommonException
class WorkerException (CommonException.CommonException):
    pass

class SharedBufferException (WorkerException):
    pass

class VideoStreamException (WorkerException):
    pass