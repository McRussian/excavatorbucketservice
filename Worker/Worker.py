__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

from time import sleep
import threading
from Logger.Logger import Logger
from tfapi.models.research.object_detection.excavator_models import Model
from Setting import SettingModel
from Worker.SharedBuffer import SharedBuffer
from Worker.WorkerException import SharedBufferException, WorkerException

class Worker (threading.Thread):
    '''
    Объект этого класса представляет собой основной поток работник.
    Его задача брать кадры с видео и отдавать их модели для анализа.
    ДЛя каждой модели создается свой собственный работник.
    Каждый работник - отдельный поток
    '''

    _model = None
    _video_buffer = None
    _result_buffer = None
    _logger = None
    _is_shutdown = False
    _classes = None
    _namemodel = None
    _description_model = None
    _analyzator = None
    _callback_stream = None

    def __init__(self, setting_model: SettingModel.SettingModel, buffer: SharedBuffer,
                 logger: Logger, frame_buffer: SharedBuffer, callback):
        threading.Thread.__init__(self)
        self._logger = logger

        self._model = Model.Model(setting_model, logger=self._logger, buffer=frame_buffer)
        self._model.Initialization()
        self._video_buffer = buffer
        self._callback_stream = callback
        self._classes = self._model.GetClassesDescription()
        self._namemodel = self._model.GetNameModel()
        self._description_model = self._model.GetDescriptionModel()

        self._result_buffer = SharedBuffer(count=5, name=self._namemodel, description=self._description_model)
        self._is_shutdown = False

    def run(self):
        while not self._is_shutdown:
            frames = None
            try:
                frames = self._video_buffer.pop()
            except SharedBufferException:
                continue
            while not frames.Empty():
                frame = None
                try:
                    frame = frames.Pop()
                except SharedBufferException:
                    continue
                try:
                    self._result_buffer.push(self._model.ClassifyFrame(image=frame))
                except SharedBufferException:
                    self._logger.PrintLog(3, 'Not Push data To Buffer')
                except Exception as err:
                    self._logger.PrintLog(13, 'Bad Analyze of Frame: ' + str(err))
                    self._callback_stream()
                    sleep(10)

    def GetResultBuffer(self)-> SharedBuffer:
        return self._result_buffer

    def Stop(self):
        self._is_shutdown = True

