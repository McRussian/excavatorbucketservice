__author__ = "McRussian Andrey"

# -*- coding: utf8 -*-

import threading
import Worker.WorkerException


class SeriesFrames:
    '''
    Объект этого класса представляет серию кадров,
    серия имеет заранее установленный размер.
    В серию можно писать кадры, и забирать их по принципу очереди.
    Так как серию будет разделять несколько потоков, то нужна синхронизация
    '''

    _size_series = None
    _series = None
    _mutex = None

    def __init__(self, size: int = 1):
        self._series = []
        self._size_series = size
        self._mutex = threading.Lock()

    def Push(self, data):
        self._mutex.acquire()
        if len(self._series) == self._size_series:
            self._mutex.release()
            raise Worker.WorkerException.VideoStreamException(21, 'Series of Frames is Full...')

        self._series.append(data)
        self._mutex.release()

    def Pop(self):
        self._mutex.acquire()
        if len(self._series) == 0:
            self._mutex.release()
            raise Worker.WorkerException.VideoStreamException(22, 'Series of Frames is Empty...')
        data = self._series.pop(0)
        self._mutex.release()

        return data

    def Empty(self) -> bool:
        self._mutex.acquire()
        rez = len(self._series) == 0
        self._mutex.release()
        return rez

    def __del__(self):
        del self._series
