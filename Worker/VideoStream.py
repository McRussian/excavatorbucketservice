__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

import threading
import time
from Worker import WorkerException
from Setting import SettingVideoStream
import cv2
from Worker import SeriesFrames
from Worker import SharedBuffer
from Logger.Logger import Logger

class VideoStream (threading.Thread):
    '''
    Объект этого класса отвечает за взаимодействие с камерой.
    Он получает настройки камеры и создает подключение к ней.
    Для взаимодействия с камерй используется OpenCV.
    Объект представляет собой отдельный поток.
    С определенной периодичностью происходит подключение к камере
    и кадр кладется в буфер, из которого воркеры смогут его забрать
    '''
    _buffer = None
    _logger = None
    _length_series = None
    _timeout_frames = None
    _timeout_series = None
    _width_frame = None
    _height_frame = None

    _is_shutdown = False
    _videocapture = None

    def __init__(self, sett: SettingVideoStream.SettingVideoStream, buffer: SharedBuffer, logger: Logger):
        threading.Thread.__init__(self)
        connection = sett.GetValuesSetting('Connection')
        if not self._connectToStream(connection):
            raise WorkerException.VideoStreamException(1, 'Not Connection to Stream. ' + connection)

        self._timeout_series = int(sett.GetValuesSetting("TimeoutBetweenSeries"))
        self._timeout_frames = int(sett.GetValuesSetting('TimeoutBetweenFrames'))
        self._length_series = int(sett.GetValuesSetting("LengthSeriesFrames"))
        self._width_frame = int(sett.GetValuesSetting('Width'))
        self._height_frame = int(sett.GetValuesSetting('Height'))

        self._logger = logger
        self._buffer = buffer


    def run(self):
        while not self._is_shutdown:
            series = SeriesFrames.SeriesFrames(self._length_series)
            for i in range(self._length_series):
                try:
                    ret, frame = self._videocapture.read()
                    frame = self._PrepareFrame(frame=frame)
                    series.Push(frame)
                except:
                    self._logger.PrintLog(code=1, msg="Don't Get Frame from VideoStream")
                    continue

                time.sleep(self._timeout_frames / 1000)
            self._buffer.push(series)
            time.sleep(self._timeout_series / 1000)


    def _PrepareFrame(self, frame):
        if self._width_frame > 0 and self._height_frame > 0:
            pass
        else:
            return frame

    def Stop(self):
        self._is_shutdown = True

    def _connectToStream(self, connect: str)-> bool:
        self._videocapture = cv2.VideoCapture(connect)
        self._videocapture.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc(*'MJPG'))
        return True

    def __del__(self):
        self._videocapture.release()
