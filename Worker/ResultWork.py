__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

class ResultWork:
    _frame = None
    _classes = None
    _score = None
    _color = None

    def __init__(self, frame, classes: str = '', scores: float = 0.0, color: str = ''):
        self._frame = frame
        self._classes = classes
        self._score = scores
        self._color = color

    def __del__(self):
        pass

    def SetClasses(self, classes: str):
        self._classes = classes

    def GetClasses(self)-> str:
        return self._classes

    def SetScores(self, scores: float):
        self._score = scores

    def GetScores(self)-> float:
        return self._score

    def GetFrame(self):
        return self._frame

    def SetColor(self, color: str):
        self._color = color

    def GetColor(self)-> str:
        return self._color