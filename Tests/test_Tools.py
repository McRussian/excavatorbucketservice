from unittest import TestCase
import Tools.Tools

class TestTools(TestCase):
    def test_PrepareStringValues(self):
        self.assertEqual(Tools.Tools.PrepareStringValues('   \n \r  helloworld\n\t   '), 'helloworld')
        self.assertEqual(Tools.Tools.PrepareStringValues('   \n \t hello world\n   '), 'hello world')
        self.assertEqual(Tools.Tools.PrepareStringValues('   \n \t hello  \t world\n   '), 'hello world')

