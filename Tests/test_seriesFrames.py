from unittest import TestCase
import Worker.SeriesFrames
import Worker.WorkerException

class TestSeriesFrames(TestCase):
    def test_Push(self):
        series = Worker.SeriesFrames.SeriesFrames(size=2)
        series.Push(1)
        series.Push(2)
        self.assertRaises(Worker.WorkerException.VideoStreamException, series.Push, 3)

    def test_Pop(self):
        series = Worker.SeriesFrames.SeriesFrames(size=2)
        series.Push(1)
        series.Push(2)
        self.assertEqual(1, series.Pop())

    def test_Empty(self):
        series = Worker.SeriesFrames.SeriesFrames(size=0)
        self.assertTrue(series.Empty())

    def test_PopFromEmpty(self):
        series = Worker.SeriesFrames.SeriesFrames()
        series.Push(1)
        data = series.Pop()
        self.assertRaises(Worker.WorkerException.VideoStreamException, series.Pop)


