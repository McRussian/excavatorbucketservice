from unittest import TestCase
import Setting.CommonSetting
import Setting.SettingVideoStream
import Setting.SettingException
import Logger

def _createSetting(filename: str) -> Setting.SettingVideoStream:
    log = Logger.Logger()
    c = Setting.CommonSetting.CommonSetting(filename=filename, logger=log)
    _sett = c.GetSettingVideoStream()
    return _sett

class TestSettingVideoStream(TestCase):
    def test_GetValues(self):
        _sett = _createSetting('setting.xml')
        self.assertEqual(_sett.GetValuesSetting('Connection'), 'rtsp://184.72.239.149/vod/mp4:BigBuckBunny_175k.mov')
        self.assertEqual(_sett.GetValuesSetting('TimeoutBetweenFrames'), '40')
        self.assertEqual(_sett.GetValuesSetting('TimeoutBetweenSeries'), '200')
        self.assertEqual(_sett.GetValuesSetting('LengthSeriesFrames'), '3')
        self.assertEqual(_sett.GetValuesSetting('Width'), '0')
        self.assertEqual(_sett.GetValuesSetting('Height'), '0')

    def test_GetNameSection(self):
        _sett = _createSetting('setting.xml')
        self.assertRaises(Setting.SettingException.SettingException, _sett.GetValuesSetting, 'Test')

    def test_ErrorNameValues(self):
        self.assertRaises(Setting.SettingException.SettingException, _createSetting, 'error_setting.xml')