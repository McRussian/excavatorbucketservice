from unittest import TestCase
from Setting import CommonSetting
from Setting import SettingModel
from Setting import ListSettingModels
from Setting import SettingException
import Logger

def _createSetting(filename: str) -> SettingModel:
    log = Logger.Logger()
    c = CommonSetting.CommonSetting(filename=filename, logger=log)
    _sett = c.GetListSettingModels()
    return _sett

class TestSettingModel(TestCase):
    def test_GetNameSection(self):
        settings = ListSettingModels.ListSettingModels()
        self.assertEqual(settings.GetNameSection(), 'ListModels')
        sett_model = SettingModel.SettingModel()
        self.assertEqual(sett_model.GetNameSection(), "SettingModel")

    def test_GetCountModels(self):
        settings = _createSetting('setting.xml')
        self.assertEqual(len(settings.GetListSettingModels()), 1)


    def test_FirstSettingModel(self):
        settings = _createSetting('setting.xml')
        sett = settings.GetListSettingModels()[0]
        self.assertEqual(sett.GetValuesSetting('NameModel'), 'Model1.1')
        self.assertEqual(sett.GetValuesSetting('DirectoryModel'), 'Models\\model1')
        self.assertEqual(sett.GetValuesSetting('Weight'), '1.0')

        self.assertRaises(SettingException.SettingException, sett.GetValuesSetting, 'Name')