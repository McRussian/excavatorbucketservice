from unittest import TestCase
import Worker.SharedBuffer
import Worker.WorkerException

class TestSharedBuffer(TestCase):
    def test_push_pop(self):
        buff = Worker.SharedBuffer.SharedBuffer(count=2)
        buff.push(1)
        self.assertEqual(1, buff.pop())
        self.assertEqual(0, len(buff))
        buff.push(1)
        buff.push(2)
        buff.push(3)
        self.assertEqual(2, len(buff))
        self.assertEqual(3, buff.pop())
        self.assertEqual(1, len(buff))

    def test_pop_empty(self):
        buff = Worker.SharedBuffer.SharedBuffer(count=2)
        buff.push(1)
        self.assertEqual(1, len(buff))
        data = buff.pop()
        self.assertEqual(1, data)
        self.assertRaises(Worker.WorkerException.SharedBufferException, buff.pop)

