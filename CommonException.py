__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

class CommonException (Exception):

    _code = None
    _message = None

    def __init__(self, code: int = 0, msg: str = ""):
        self._code = code
        self._message = msg

    def GetErrorCode(self)-> int:
        return  self._code

    def GetErrorMessage(self)-> str:
        return self._message