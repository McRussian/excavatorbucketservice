__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

import cv2
from PyQt5.QtWidgets import QMainWindow, QLabel
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtCore import pyqtSignal, QObject, Qt, QTimer
from GUI.WidgetBucket import WidgetBucket
from GUI.WorkerResult import WorkerResult
from Worker.SharedBuffer import SharedBuffer


class CloseScreenSignal(QObject):
    # создаем свой сигнал
    close_screen_signal = pyqtSignal()


class MainScreen(QMainWindow):
    '''
    Объект этого класса представляет собой экран, который будет показывать
    видео с камеры, сам ковш и результаты распознования ковша
    '''
    _screen = None
    _width_screen = None
    _height_screen = None
    _widgets_analyz: dict = None
    _names_models = None
    _timer: QTimer = None
    _analyzators: dict = None
    _buffer = None
    close_signal = None

    def __init__(self, width: int = 800,  height: int = 600):
        super().__init__()
        self._width_screen = width
        self._height_screen = height
        self._widgets_analyz = dict()
        self._analyzators = dict()
        self.close_signal = CloseScreenSignal()

        # Создаем главное окно и его свойства
        self.setWindowFlag(Qt.FramelessWindowHint)
        self.setMinimumWidth(self._width_screen)
        self.setMinimumHeight(self._height_screen)
        # self.setWindowTitle('Определение целостности ковша экскаватора')
        self._InitUI()

    def Start(self):
        self.show()
        self._timer.start()

    def Stop(self):
        self.close()
        self._timer.stop()

    def SetModels(self, names: list, buffer: dict):
        self._names_models = names
        for name in names:
            self._widgets_analyz[name] = WidgetBucket(self._width_screen, 100, 6, self)
            self._widgets_analyz[name].setGeometry(0, 0, self._width_screen, 100)
            self._analyzators[name] = WorkerResult(name=name, buffer=buffer[name])


    def _InitUI(self):
        self._screen = QLabel(self)
        self._screen.setGeometry(0, 0, self._width_screen, self._height_screen)
        self._timer = QTimer()
        self._timer.setInterval(100)
        self._timer.timeout.connect(self.HandlerWorkerRezult)

    def closeEvent(self, event):
        self.close_signal.close_screen_signal.emit()

    def ShowFrame(self, frame: cv2):
        dsize = self._screen.width(), self._screen.height()
        try:
            output = cv2.resize(frame, dsize)
            convertToQtFormat = QImage(output.data, output.shape[1], output.shape[0], QImage.Format_RGB888)
            convertToQtFormat = QPixmap.fromImage(convertToQtFormat)
            pixmap = QPixmap(convertToQtFormat)
            self._screen.setPixmap(pixmap)
        except:
            pass

    def HandlerWorkerRezult(self):
        for name in self._names_models:
            if self._analyzators[name].Check():
                self.ShowEvent(name=name)

    def ShowEvent(self, name: str):
        self._widgets_analyz[name].StartEventShow()
