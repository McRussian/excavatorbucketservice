from PyQt5.QtWidgets import QWidget, QLabel
from PyQt5.QtCore import Qt, QTimer

class WidgetBucket(QWidget):
    _count: int = None
    _color_common: str = None
    _color_event: str = None
    _tick: int = None
    _thooth_labels: list = None
    _timer: QTimer = None
    _enable_event: bool = None
    _current_tick: int = None

    def __init__(self, width: int, height: int, count: int, parent: QWidget = None):
        super().__init__()
        self.setParent(parent)
        self.setFixedWidth(width)
        self.setFixedHeight(height)

        self._tick = 5
        self._count = count
        self._thooth_labels = list()
        self._timer = QTimer()
        self._timer.setInterval(1500)
        self._timer.timeout.connect(self.ChangeColorLabels)
        self._enable_event = False
        self._current_tick = 0
        self._initUI()


    def _initUI(self):
        self._color_common = '#6AD2A2'
        self._color_event = '#EB0020'
        self.setAutoFillBackground(True)
        p = self.palette()
        p.setColor(self.backgroundRole(), Qt.gray)
        self.setPalette(p)
        left = center_x = self.width() // 2
        height = self.height() - 20
        bottom = (self.height() - height) // 2
        space = 30
        width = 40
        if self._count % 2 == 1:
            label = QLabel(self)
            label.setGeometry(left - width // 2, bottom, width, height)
            self._thooth_labels.append(label)
            left = center_x - width // 2 - space
        else:
            left = center_x - space // 2

        for step in range(self._count // 2):
            label_left = QLabel(self)
            label_left.setGeometry(left - width, bottom, width, height)
            self._thooth_labels.append(label_left)

            label_right = QLabel(self)
            label_right.setGeometry(self.width() - left, bottom, width, height)
            self._thooth_labels.append(label_right)
            left = left - width - space

        for label in self._thooth_labels:
            label.setStyleSheet("background-color: %s;" % self._color_common)

    def StartEventShow(self):
        if self._enable_event:
            return

        self._timer.start()
        self._enable_event = True

    def ChangeColorLabels(self):
        if self._current_tick % 2 == 0:
            for label in self._thooth_labels:
                label.setStyleSheet("background-color: %s;" % self._color_event)
        else:
            for label in self._thooth_labels:
                label.setStyleSheet("background-color: %s;" % self._color_common)

        if self._current_tick == 2 * self._tick + 1:
            self._timer.stop()
            self._enable_event = False
            self._current_tick = 0

        self._current_tick = self._current_tick + 1

