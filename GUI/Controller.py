__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

from PyQt5.QtCore import pyqtSignal, QObject
from GUI.Screen import MainScreen
from GUI.WorkerVideo import WorkerVideo
from Logger import Logger
from Worker.SharedBuffer import SharedBuffer

class ControllerScreen(QObject):
    _window: MainScreen = None
    _buffer: SharedBuffer = None
    _results_buffer: dict = None
    _logger: Logger = None
    _videoworker: WorkerVideo = None

    _callback_close = None

    def __init__(self, buffer: SharedBuffer, logger: Logger, closecallback = None):
        super().__init__()
        self._window = MainScreen()
        self._buffer = buffer
        self._logger = logger
        self._callback_close = closecallback
        self._videoworker = WorkerVideo(screen=self._window, buffer=self._buffer)

        self._window.close_signal.close_screen_signal.connect(self.Stop)

    def Start(self):
        self._logger.PrintLog(101, 'Initialization GUI')
        names = list(self._results_buffer.keys())
        self._window.SetModels(names=names, buffer=self._results_buffer)
        self._window.Start()
        self._videoworker.start()

    def Stop(self):
        self._videoworker.Stop()
        self._window.Stop()
        self._logger.PrintLog(111, 'GUI Closed')

        self._callback_close()

    def SetResultsBuffer(self, buffers: dict):
        self._results_buffer = buffers