__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

import threading
from time import sleep

from Worker.WorkerException import SharedBufferException
from Worker.SharedBuffer import SharedBuffer
from GUI.Screen import MainScreen


class WorkerVideo(threading.Thread):
    _buffer: SharedBuffer = None
    _screen: MainScreen = None
    _is_shutdown: bool = False

    def __init__(self, screen: MainScreen, buffer: SharedBuffer):
        threading.Thread.__init__(self)
        self._screen = screen
        self._buffer = buffer

    def run(self):
        while not self._is_shutdown:
            frames = None
            try:
                frames = self._buffer.pop()
            except SharedBufferException:
                continue
            while not frames.Empty():
                frame = None
                try:
                    frame = frames.Pop()
                except SharedBufferException:
                    continue
                self._screen.ShowFrame(frame)
                sleep(0.04)

    def Stop(self):
        self._is_shutdown = True