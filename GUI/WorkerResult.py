__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

from Worker.WorkerException import SharedBufferException
from Worker.SharedBuffer import SharedBuffer
from Worker.ResultWork import ResultWork

class WorkerResult:
    _name: str = None
    _buffer: SharedBuffer = None

    def __init__(self, name: str, buffer: SharedBuffer):
        self._name = name
        self._buffer = buffer

    def Check(self) -> bool:
        result: ResultWork = None
        try:
            result: ResultWork = self._buffer.pop()
        except SharedBufferException:
            return False

        p: int = int(result.GetScores() * 100)
        color = result.GetColor()
        if color == 'red' and p > 50:
            return True
