__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-


from PyQt5.QtWidgets import QApplication
from Setting import CommonSetting
import os, sys
from Worker.SharedBuffer import SharedBuffer


if __name__ == '__main__':
    app = QApplication(sys.argv)

    key = "PYTHONPATH"
    dirname = os.path.abspath(os.curdir) + os.sep + 'tfapi' + os.sep + 'models' + os.sep + 'research'
    os.environ[key] = dirname + ':' + dirname + os.sep + 'slim'
    from Worker import Controller
    from Worker import WorkerException
    from Logger.ControllerLogger import ControllerLogger

    setting = CommonSetting.CommonSetting(filename='setting.xml')

    logger = ControllerLogger(setting=setting.GetSettingLogger())
    buffer: SharedBuffer = logger.GetBuffer()
    controller = Controller.Controller(setting=setting, logger=logger.GetLogger())

    try:
        controller.Initizialization(logger.Stop)
    except WorkerException as err:
        print(err.GetMessage())

    timeout = setting.GetTimeOutSetting()
    try:
        controller.Start(buffer=buffer)
    except Exception as err:
        logger.GetLogger().PrintLog(4, err.__str__())

    sys.exit(app.exec_())
