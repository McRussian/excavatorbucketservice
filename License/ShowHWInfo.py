__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

import cpuinfo

class HWinfo:
    '''
    Объект этого класса отвечает за формирование строки информации о железе
    данного компьютера.
    Опрашивается версия и модель цпу, материнской платы, видеокарты, оперативной памяти и HDD.
    Эта строка и возвращается как результат работы класса
    '''

    _info = ''

    def __init__(self):
        self._info += "BIOS:" + self._BIOSInfo() + "||"
        self._info += "CPU:" + self._CPUInfo() + "||"
        self._info += "MB:" + self._MatherBoardInfo() + "|"

    def GetInformation(self)-> str:
        return self._info

    def _BIOSInfo(self)-> str:
        name_keys = ['Version', 'Manufacturer', 'SMBIOSBIOSVersion']
        info = ''

        return info

    def _CPUInfo(self)-> str:
        name_keys = ['arch', 'bits', 'count', 'raw_arch_string', 'vendor_id', 'brand',
                     'model', 'family', 'l3_cache_size', 'l2_cache_size', 'l1_data_cache_size',
                     'l1_instruction_cache_size']
        info_dict = cpuinfo.get_cpu_info()
        info = ''
        for key in name_keys:
            info += key + ':' + str(info_dict[key]) + ';'
        return info

    def _GPUInfo(self)-> str:
        return ""

    def _MatherBoardInfo(self)-> str:
        name_keys = ['Manufacturer', 'Product', 'Version', 'SerialNumber']
        info = ''
        return info

    def _MemoryInfo(self)-> str:
        return ""

    def _HDDInfo(self)-> str:
        return ""