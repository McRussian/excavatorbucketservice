__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

from License import ShowHWInfo
from Setting import SettingLicense
from Setting import CommonSetting
from . import LicenseException

class License:
    '''
    Объект этого класса отвечает за проверку лицензии
    Его задача проверить конфигурацию системы и составить из нее хеш
    Если запуск производится первый раз, то данный хеш записывается в конфиг
    Если не первый раз, то проверяется тот хеш, который был в конфиге
    с тем что был получен сейчас...
    '''

    _current_hash = None
    _setting = None
    _sett_license = None

    def __init__(self, setting: CommonSetting):
        self._setting = setting
        self._sett_license = self._setting.GetSettingLicense()
        self._current_hash = self._sett_license.GetHash()


    def CheckLicense(self):
        shower = ShowHWInfo.HWinfo()
        info = shower.GetInformation()
        print(info)
        print(hash(info))
        if self._current_hash == self._sett_license.GetDefaultHash():
            self._setting.ChangeHashInLicense(str(hash(info)))

        if self._current_hash != hash(info):
            raise LicenseException.LicenseException(code=10, msg='Invalid License')





