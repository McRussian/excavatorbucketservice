__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

import re

'''
Здесь собраны функции, которые будут использоваться
в разных местах всего сервиса
'''

def PrepareStringValues(s: str)-> str:
    '''
    Функция, которая обрабатывает строку
    происходит удаление начальных, конечных пробелов, спецсимволов и конца строки
    :param s: строка, которую нужно подготовить
    :return: обработанная строка
    '''
    s = s.strip()
    pattern = '[\n\t\r\ ]'
    s = re.sub(pattern, ' ', s)
    s = re.sub('\s+', ' ', s)
    return s.strip()