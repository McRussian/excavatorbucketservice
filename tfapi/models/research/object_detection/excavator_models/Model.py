from numpy import squeeze, expand_dims, int32
import os
import tensorflow as tf
from tfapi.models.research.object_detection.utils import label_map_util
from tfapi.models.research.object_detection.utils import visualization_utils as vis_util
from os import sep
from Setting import SettingModel
from Logger.Logger import Logger
from Worker.Analyzator import *

class Model:
    '''
    Объект этого класса представляет интерфейс для взаимодейтсвия с моделью
    Его задачи
    1. Загрузить модель из файла
    2. Принять на анализ кадр и вернуть результат анализа
    '''

    _modelName = None  # Название модели
    _decriptionModel = None
    _modelDirectory = None
    _detectionGraph = None  # Граф модели для Tensorflow
    _sess = None  # Сессия для Tensorflow
    _PATH_TO_LABELS = None  # Путь к файлу с классами, добавляемый в переменные окружения системы
    _categoryIndex = None  # Идентификатор класса
    _analyzator = None
    _logger = None

    def __init__(self, setting: SettingModel.SettingModel, logger: Logger, buffer: SharedBuffer):
        self._modelName = setting.GetValuesSetting('NameModel')
        self._decriptionModel = setting.GetValuesSetting('DescriptionModel')
        self._modelDirectory = setting.GetValuesSetting('DirectoryModel') + sep
        self._logger = logger
        self._analyzator = Analyzator[self._modelName](buffer)

    def Initialization(self):
        # Путь к модели
        PATH_TO_FROZEN_GRAPH = self._modelDirectory + 'frozen_inference_graph.pb'

        self._detectionGraph = tf.Graph()
        with self._detectionGraph.as_default():
            od_graph_def = tf.compat.v1.GraphDef()
            with tf.io.gfile.GFile(PATH_TO_FROZEN_GRAPH, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')

        self._sess = tf.compat.v1.Session(graph=self._detectionGraph)
        self._detectionGraph.as_default()

        self._PATH_TO_LABELS = os.path.join(self._modelDirectory, 'label_map.pbtxt')
        self._categoryIndex = label_map_util.create_category_index_from_labelmap(self._PATH_TO_LABELS,
                                                                                 use_display_name=True)
        self._logger.PrintLog(0, "Model successfully loaded")

    def GetClassesDescription(self)-> dict:
        classes = dict()
        for key in self._categoryIndex.keys():
            classes[key] = self._categoryIndex[key]['name']

        return classes

    def GetNameModel(self)-> str:
        return self._modelName

    def GetDescriptionModel(self)-> str:
        return self._decriptionModel

    def ClassifyFrame(self, image, threshold: float = 0.4)-> ResultWork:
        result = ResultWork(image)
        image_np = image
        image_np_expanded = expand_dims(image_np, axis=0)
        image_tensor = self._detectionGraph.get_tensor_by_name('image_tensor:0')
        boxes = self._detectionGraph.get_tensor_by_name('detection_boxes:0')
        scores = self._detectionGraph.get_tensor_by_name('detection_scores:0')
        classes = self._detectionGraph.get_tensor_by_name('detection_classes:0')
        num_detections = self._detectionGraph.get_tensor_by_name('num_detections:0')

        # Непосредственно детекция и классификация
        (boxes, scores, classes, num_detections) = self._sess.run([boxes, scores, classes, num_detections],
                                                              feed_dict={image_tensor: image_np_expanded})
        # Визуализация боксов и текста
        vis_util.visualize_boxes_and_labels_on_image_array(
                image_np,
                squeeze(boxes),
                squeeze(classes).astype(int32),
                squeeze(scores),
                self._categoryIndex,
                use_normalized_coordinates=True,
                line_thickness=8,
                min_score_thresh=threshold)
        result.SetClasses(self._categoryIndex[classes[0][0]]['name'])
        result.SetScores(scores[0][0])
        color = self._analyzator.Analyze(result)
        result.SetColor(color)
        return result