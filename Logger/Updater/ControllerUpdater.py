__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

import datetime
import os
from Logger.Logger import Logger
from Logger.Updater.BackupFrame import BackupFrame
from Logger.Updater.CheckUSB import CheckUSB


class ControllerUpdater:
    _backup: BackupFrame = None
    _src_backup_dir: str = None
    _logger: Logger = None
    _timeout: int = 0
    _conf: str = None
    _level: int = None
    _dates: list = None
    _format: str = "%Y.%m.%d-%H.%M.%S.%f"
    _checker: CheckUSB = None

    def __init__(self, logger: Logger, conf: str, level: int, src: str, timeout: int):
        self._logger = logger
        self._conf = os.path.abspath(os.path.curdir) + os.sep + conf
        self._SetDatesBackup()
        self._level = level
        self._src_backup_dir = os.path.abspath(os.path.curdir) + os.sep + src
        self._timeout = timeout
        self._checker = CheckUSB('BackupFrame', '/dev/sdc1', self._timeout, self._SetDestinationBackup)

    def Enable(self):
        self._checker.start()

    def _SetDatesBackup(self):
        self._dates = list()
        try:
            with open(self._conf) as fin:
                rows = fin.readlines()
                if len(rows) == 0:
                    self._dates.append(datetime.datetime(2020, 1, 1, 0, 0, 0))
                else:
                    for row in rows:
                        date = datetime.datetime.strptime(row[: -1], self._format)
                        self._dates.append(date)
        except:
            self._dates = [datetime.datetime(2020, 1, 1, 0, 0, 0)]

    def _SetDestinationBackup(self, dst: str):
        if len(self._dates) == 0:
            now = datetime.datetime(2020, 1, 1, 0, 0, 0)
        else:
            now = self._dates[-1]
        date = now.date()
        time = now.time()
        self._backup = BackupFrame(self._logger, self._src_backup_dir, dst, date, time, self._StopBackup)
        self._backup.start()

        self._dates.append(datetime.datetime.now())
        self._RotateBackup()
        self._SaveConfBackup()

    def _RotateBackup(self):
        if len(self._dates) > self._level:
            date = self._dates.pop(0)

    def _SaveConfBackup(self):
        with open(self._conf, 'w') as fout:
            for date in self._dates:
                fout.write(date.strftime(self._format) + "\n")

    def _StopBackup(self):
        self._checker.UmountUSB()

    def Stop(self):
        self._checker.Stop()