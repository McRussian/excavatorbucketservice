__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

import os
import shutil
import datetime
from threading import Thread
from Logger.Logger import Logger


class BackupFrame(Thread):
    _srcdir: str = None
    _dstdir: str = None
    _logger: Logger = None
    _begin: str = None
    _callback = None

    def __init__(self, logger: Logger, srcdir: str, dstdir: str,
                 date: datetime.date, time: datetime.time,
                 callback):
        Thread.__init__(self)
        self._logger = logger
        self._srcdir = srcdir
        self._dstdir = dstdir
        self._begin = date.strftime("%Y.%m.%d") + '-' + time.strftime("%H.%M.%S.%f") + '.jpg'
        self._callback = callback

    def _MakeInnerDirectory(self, srcdir: str, fulldirname: str):
        dstdirname = srcdir
        for dirname in fulldirname.split('/'):
            dstdirname = dstdirname + os.sep + dirname
            if not os.path.exists(dstdirname):
                os.mkdir(dstdirname)

    def run(self):
        old_dirname = os.path.abspath(os.path.curdir)
        os.chdir(self._srcdir)
        self._logger.PrintLog(201, 'Backup Frame Start')
        end = datetime.date.today().strftime("%Y.%m.%d") + '-' + \
              datetime.datetime.now().time().strftime("%H.%M.%S.%f") + '.jpg'
        for dirname, childs, files in os.walk(os.path.curdir):
            if len(files) > 0:
                self._MakeInnerDirectory(srcdir=self._dstdir, fulldirname=dirname)
                for filename in files:
                    if filename > self._begin and filename < end:
                        fullsrcfilename = self._srcdir + os.sep + os.path.normpath(dirname) + os.sep + filename
                        fulldstfilename = self._dstdir + os.sep + os.path.normpath(dirname) + os.sep + filename
                        try:
                            shutil.copy(fullsrcfilename, fulldstfilename)
                        except Exception as err:
                            self._logger.PrintLog(213, 'File {} not Copy to file {}.'.format(fullsrcfilename, fulldstfilename) +
                                                  ' ' + str(err))
        os.chdir(old_dirname)
        self._logger.PrintLog(202, 'Backup Frame Stop')
        self._callback()