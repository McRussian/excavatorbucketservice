__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

import datetime
import os
import subprocess
from time import sleep
from threading import Thread


class CheckUSB (Thread):
    _dirname: str = None
    _device: str = None
    _timeout: int = None
    _is_shutdown: bool = False
    _callback_find = None
    _date_backup: datetime.date = None
    _is_backup: bool = False

    def __init__(self, dirname: str, device: str, timeout: int, callback):
        Thread.__init__(self)
        self._dirname = dirname
        self._device = device
        self._timeout = timeout
        self._callback_find = callback
        self._date_backup = datetime.date.today()

    def _mountUSB(self) -> bool:
        result = subprocess.run(['sudo', '/bin/mount', '-o', 'umask=000,user,noauto', self._device, '/mnt/usb'])
        if result.returncode == 0:
            subprocess.run(['sudo', '/bin/chmod', '777', '/mnt/usb'])
            dirname = '/mnt/usb' + os.sep + self._dirname
            if not os.path.exists(dirname):
                os.mkdir(dirname)
            return True
        return False

    def run(self):
        while not self._is_shutdown:
            if not self._is_backup and self._mountUSB():
                self._date_backup = datetime.date.today()
                self._is_backup = True
                self._callback_find('/mnt/usb' + os.sep + self._dirname)
            else:
                if not self._date_backup == datetime.date.today():
                    self._is_backup = False

            sleep(self._timeout)

    def UmountUSB(self) -> bool:
        result = subprocess.run(['sudo', '/bin/umount', self._device])
        return result.returncode == 0

    def start(self) -> None:
        super().start()

    def Stop(self):
        self._is_shutdown = True