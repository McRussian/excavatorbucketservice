__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-


class DataFrameLog:
    _frame = None
    _namemodel: str = None
    _classes: str = None

    def __init__(self, frame, namemodel: str, classes: str):
        self._frame = frame
        self._namemodel = namemodel
        self._classes = classes

    def GetFrame(self):
        return self._frame

    def GetNameModel(self)-> str:
        return self._namemodel

    def GetClasses(self)-> str:
        return self._classes

    def __del__(self):
        del self._frame