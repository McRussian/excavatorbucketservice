__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

from threading import Thread, Lock
import datetime
import Definitions
import os
import cv2
from time import sleep

from Logger.DataFrameLog import DataFrameLog as Data
from Worker.SharedBuffer import SharedBuffer
from Worker.WorkerException import SharedBufferException

class FrameLogger(Thread):
    _frame_buffer: SharedBuffer = None
    _dirname_pict: str = None
    _is_shutdown: bool = False
    _mutex: Lock = None
    _enable: bool = True

    def __init__(self, dirname: str = 'frames'):
        Thread.__init__(self)
        self._frame_buffer = SharedBuffer(10)
        self._dirname_pict = Definitions.ROOT_DIR + os.sep + dirname
        if not os.path.exists(self._dirname_pict):
            os.mkdir(self._dirname_pict)
        self._mutex = Lock()

    def Disable(self):
        self._enable = False
        self.Stop()

    def run(self):
        if not self._enable:
            return
        while not self._is_shutdown:
            data = None
            try:
                data = self._frame_buffer.pop()
            except SharedBufferException:
                sleep(0.04)
                continue
            namemodel: str = data.GetNameModel()
            if not os.path.exists(self._dirname_pict + os.sep + namemodel):
                os.mkdir(self._dirname_pict + os.sep + namemodel)
            classes: str = data.GetClasses()
            dirname = self._dirname_pict + os.sep + namemodel + os.sep + classes + os.sep
            if not os.path.exists(dirname):
                os.mkdir(dirname)
            self._mutex.acquire()
            date = datetime.date.today()
            time = datetime.datetime.now().time()
            filename = dirname + date.strftime("%Y.%m.%d") + '-' + time.strftime("%H.%M.%S.%f") + '.jpg'
            try:
                cv2.imwrite(filename=filename, img=data.GetFrame())
            except:
                print('Don\'t Save Filename: ' + filename)
            self._mutex.release()
            sleep(0.04)

    def Stop(self):
        self._is_shutdown = True

    def GetBuffer(self)-> SharedBuffer:
        return self._frame_buffer


