__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

from Setting.SettingTools import SettingTools
from Logger.Logger import Logger
from Logger.FrameLogger import FrameLogger
from Logger.Updater.ControllerUpdater import ControllerUpdater as Backup
from Setting.SettingException import SettingException
from threading import Lock
from Worker.SharedBuffer import SharedBuffer

class ControllerLogger:
    _setting_logger = None
    _logrotate = None
    _logger = None
    _frame_logger: FrameLogger = None
    _mutex = None
    _backup: Backup = None

    def __init__(self, setting: SettingTools = None):
        if setting is None:
            logfile = 'logfile.txt'
            framesdir = 'frames'
            timeout: str = '30'
            backup_conf: str = 'backup.conf'
            level: str = '3'
        else:
            try:
                logfile = setting.GetValuesSetting('LogFile')
                framesdir = setting.GetValuesSetting('FramesDir')
                timeout = setting.GetValuesSetting('Timeout')
                backup_conf = setting.GetValuesSetting('BackupConf')
                level = setting.GetValuesSetting('Level')
            except SettingException as err:
                logfile = 'logfile.txt'
                framesdir = 'frames'
                timeout = '30'
                backup_conf: str = 'backup.conf'
                level: str = '3'

        self._logger = Logger(filename=logfile)
        self._frame_logger = FrameLogger(dirname=framesdir)
        self._mutex = Lock()
        self._logger.SetMutex(self._mutex)
        self._frame_logger.start()
        self._backup = Backup(logger=self._logger, conf=backup_conf, level=int(level),
                              src=framesdir, timeout=int(timeout))


    def DisableLogging(self):
        self._logger.Disable()

    def EnableAutoBackup(self):
        self._backup.Enable()

    def GetLogger(self)-> Logger:
        return self._logger

    def GetBuffer(self)-> SharedBuffer:
        return self._frame_logger.GetBuffer()

    def Stop(self):
        self._frame_logger.Stop()
        self._backup.Stop()
