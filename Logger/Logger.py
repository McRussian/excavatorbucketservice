__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

from threading import Lock
import datetime
import Definitions
import os

class Logger:
    '''
    Объект этого класса отвечает за запись лог-сообщений в лог-файл
    Объект будет передаваться в другие объекты, которые будут его использовать
    '''

    _filename_logfile = None
    _mutex = None
    _logger = None
    _dirname_pict = None
    _enable: bool = True

    def __init__(self, filename: str = 'logfile.txt'):
        self._filename_logfile = Definitions.ROOT_DIR + os.sep + filename

    def SetMutex(self, mutex: Lock):
        self._mutex = mutex

    def Disable(self):
        self._enable = False

    def PrintLog(self, code: int, msg: str):
        if not self._enable:
            return
        self._mutex.acquire()
        logfile = open(self._filename_logfile, 'a')
        date = datetime.date.today()
        time = datetime.datetime.now().time()
        row = date.strftime("%Y.%m.%d") + ' ' + time.strftime("%H.%M.%S.%f") + '.    Level ' + str(code) + ': ' + msg + '\n\r'
        logfile.write(row)
        logfile.close()
        self._mutex.release()

