__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

import xml.etree.ElementTree as ET
from Setting import SettingException
import Tools.Tools


class SettingTools:
    '''
    Объект этого класса отвечает за настройку класс Логгер.
    Объект класса логгер записывает информацию о работе системы в лог-файл.
    Название лог-файла задается в настройках.
    Кроме этого, в настройках задается уровень ротации (daily, weekly, monthly)
    И каталог. в который будет сохраняться архив с логами за указанный период
    '''
    _logfile = ''
    _rotate = None
    _bacupdir = ''
    _name_section = 'SettingTools'
    _values_setting = None
    _name_values = ['LogFile', 'FramesDir', 'Timeout', 'BackupConf', 'Level']

    def __init__(self):
        self._values_setting = dict()

    def SetValuesSetting(self, node: ET.Element):
        if node == None:
            raise SettingException.SettingException(51, 'Not Setting for Logger')

        for item in node:
            self._values_setting[item.tag] = Tools.Tools.PrepareStringValues(item.text)

        for name in self._name_values:
            if not name in self._values_setting.keys():
                raise SettingException.SettingException(53, 'Not Setting for Logger. ' + name)

    def GetNameSection(self)-> str:
        return self._name_section

    def GetNameValues(self)-> list:
        return self._name_values

    def GetValuesSetting(self, name: str)-> str:
        if name in self._name_values:
            return self._values_setting[name]
        else:
            raise SettingException.SettingException(15, 'Not Valid Name for NameSetting. ' + name)