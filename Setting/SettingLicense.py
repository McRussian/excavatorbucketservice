__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

import xml.etree.ElementTree as ET
from Setting import SettingException
import datetime

class SettingLicense:

    _hash = None
    _date = None

    _name_section = 'SettingLicense'


    def __init__(self):
        pass

    def GetNameSection(self)-> str:
        return self._name_section

    def GetHash(self)-> str:
        return self._hash

    def SetHash(self, hash: str):
        self._hash = hash

    def GetDefaultHash(self)-> str:
        return 'Empty Hash'

    def SetValuesSetting(self, node: ET.Element):
        if node == None:
            raise SettingException.SettingException(code=31, msg='Not Node for License')

        self._date = node.find('Date').text
        self._hash = node.find('Hash').text


    def GetDefaultNode(self)-> ET.Element:
        node = ET.Element(self._name_section)
        date = ET.SubElement(node, 'Date')
        date.text = datetime.datetime.now().strftime("%Y-%m-%d")
        hash = ET.SubElement(node, 'Hash')
        hash.text = self.GetDefaultHash()

        return node

    def GetSettingNode(self)-> ET.Element:
        node = ET.Element(self._name_section)
        date = ET.SubElement(node, 'Date')
        date.text = datetime.datetime.now().strftime("%Y-%m-%d")
        hash = ET.SubElement(node, 'Hash')
        hash.text = self._hash

        return node
