__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

import Setting.SettingException
import xml.etree.ElementTree as ET
import Tools.Tools



class SettingVideoStream:
    '''
    Объект этого класса хранит настрйки видеопотока, а именно
    connection - строку подключения к камере (используется opencv)
    framesps - количество кадров в секунду, которые получаются с камеры
    width - ширина кадра
    height - высота кадра

    '''
    _name_section = 'SettingVideoStream'
    _values_setting = None
    _name_values = ['Connection', 'TimeoutBetweenFrames', 'LengthSeriesFrames',
                   'TimeoutBetweenSeries', 'Width', 'Height']
    def __init__(self):
        self._values_setting = dict()

    def GetNameValues(self)-> list:
        return self._name_values

    def GetValuesSetting(self, name: str)-> str:
        if name in self._name_values:
            return self._values_setting[name]
        else:
            raise Setting.SettingException.SettingException(15, 'Not Valid Name for NameSetting. ' + name)

    def GetNameSection(self)-> str:
        return self._name_section

    def SetValuesSetting(self, node: ET.Element):
        if node == None:
            raise Setting.SettingException.SettingException(11, 'Not Setting for VideoStream')

        for item in node:
            self._values_setting[item.tag] = Tools.Tools.PrepareStringValues(item.text)

        for name in self._name_values:
            if not name in self._values_setting.keys():
                raise Setting.SettingException.SettingException(13, 'Not Setting for VideoStream. ' + name)