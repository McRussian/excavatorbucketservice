__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

from Setting import SettingException
import xml.etree.ElementTree as ET
import Tools.Tools


class SettingModel:
    '''
    Объект этого класса отвечает за настройки модели.

    '''

    _name_section = 'SettingModel'
    _values_setting = None
    _name_values = ['NameModel', 'DescriptionModel', 'DirectoryModel', 'Weight']

    def __init__(self):
        self._values_setting = dict()

    def GetNameSection(self)-> str:
        return self._name_section

    def GetNameValues(self)-> list:
        return self._name_values

    def SetValuesSetting(self, node: ET.Element):
        if node == None:
            raise SettingException.SettingException(25, 'Not Setting for Model')

        if node.tag != self._name_section:
            raise SettingException.SettingException(26, 'Not Valid Node for Setting Model')

        for item in node:
            self._values_setting[item.tag] = Tools.Tools.PrepareStringValues(item.text)

        for name in self._name_values:
            if not name in self._values_setting.keys():
                raise SettingException.SettingException(33, 'Not Setting for Model. ' + name)

    def GetValuesSetting(self, name: str)-> str:
        if name in self._name_values:
            return self._values_setting[name]
        else:
            raise SettingException.SettingException(15, 'Not Valid Name for NameSetting. ' + name)