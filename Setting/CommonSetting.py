__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

from Setting import SettingVideoStream
from Setting import SettingException
from Setting import ListSettingModels
from Setting import SettingLicense
from Setting import SettingTools
import xml.etree.ElementTree as ET
from Logger.Logger import Logger
import os
import Definitions

class CommonSetting:
    '''
    Объект этого класса отвечает за чтение конфигурации сервиса
    Считываются параметры из xml файла, и создаются объекты для настройки
    разных частей сервиса.
    Хранятся настройки для:
    системы лицензирования - класс SettingLicense
    системы видеонаблюдения - класс SettingVideoStream
    системы аналитилки - класс SettingModel
    системы логирования - класс SettingLogger
    '''

    _logger = None
    _filename_setting = None
    _setting_videostream = None
    _settings_models = None
    _setting_license = None
    _setting_timeout = None
    _setting_logger = None

    _root = None

    def __init__(self, filename: str):
        self._filename_setting = Definitions.ROOT_DIR + os.sep + filename
        self._logger = None
        self._root = ET.parse(self._filename_setting).getroot()
        self._CreateSettingVideoStrem()
        self._CreateSettingLicense()
        self._CreateListSettingsModels()
        self._CreateSettingTimeOut()
        self._CreateSettingLogger()

    def GetTimeOutSetting(self)-> int:
        return self._setting_timeout

    def GetSettingVideoStream(self)-> SettingVideoStream.SettingVideoStream:
        return self._setting_videostream

    def GetSettingLicense(self)-> SettingLicense.SettingLicense:
        return self._setting_license

    def GetListSettingModels(self)-> ListSettingModels.ListSettingModels:
        return self._settings_models

    def GetSettingLogger(self)->SettingTools.SettingTools:
        return self._setting_logger

    def _CreateSettingVideoStrem(self):
        self._setting_videostream = SettingVideoStream.SettingVideoStream()
        node = self._root.find(self._setting_videostream.GetNameSection())
        try:
            self._setting_videostream.SetValuesSetting(node)
        except SettingException.SettingException:
            self._logger.PrintLog(11, "Not Section with Setting for VideoStream")
            raise SettingException.SettingException(1, "Error Setting. Not section for VideoStream")

    def _CreateListSettingsModels(self):
        self._settings_models = ListSettingModels.ListSettingModels()
        node = self._root.find(self._settings_models.GetNameSection())
        try:
            self._settings_models.SetValuesSetting(node)
        except SettingException.SettingException:
            self._logger.PrintLog(15, "Not Section with Setting for ListSettingModels")
            raise SettingException.SettingException(1, "Error Setting. Not section for ListSetting")

    def _CreateSettingLogger(self):
        self._setting_logger = SettingTools.SettingTools()
        node = self._root.find(self._setting_logger.GetNameSection())
        try:
            self._setting_logger.SetValuesSetting(node)
        except:
            pass

    def _CreateSettingLicense(self):
        self._setting_license = SettingLicense.SettingLicense()
        node = self._root.find(self._setting_license.GetNameSection())
        try:
            self._setting_license.SetValuesSetting(node)

        except SettingException.SettingException:
            self._logger.PrintLog(31, "Not Section with Setting for License")
            self._root.append(self._setting_license.GetDefaultNode())
            ET.dump(self._root)
            tree = ET.ElementTree()
            tree._setroot(self._root)
            tree.write(self._filename_setting)

    def _CreateSettingTimeOut(self):
        node = self._root.find('TimeOut')
        if node == None:
            self._setting_timeout = 0
        else:
            try:
                self._setting_timeout = int(node.text)
            except:
                self._setting_timeout = 0

    def ChangeHashInLicense(self, hash: str):
        self._setting_license.SetHash(hash=hash)
        self._root.remove(self._root.find(self._setting_license.GetNameSection()))
        self._root.append(self._setting_license.GetSettingNode())
        ET.dump(self._root)
        tree = ET.ElementTree()
        tree._setroot(self._root)
        tree.write(self._filename_setting)

