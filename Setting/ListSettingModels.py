__author__ = "McRussian Andrey"
# -*- coding: utf8 -*-

from Setting import SettingModel
from Setting import SettingException
import xml.etree.ElementTree as ET
import Tools.Tools

class ListSettingModels:
    '''
    Объект этого класса отвечает за создание настроек для всех моделей,
    описанных в файле конфигурации.
    Принимает в себя часть xml-документа и создает список настроек...
    '''

    _name_section = 'ListModels'
    _ls_setting_models = None

    def __init__(self):
        self._ls_setting_models = list()

    def GetListSettingModels(self)-> list:
        if len(self._ls_setting_models) == 0:
            raise SettingException.SettingException(22, 'Not Setting for ListModels...')
        return self._ls_setting_models

    def GetNameSection(self)-> str:
        return self._name_section

    def SetValuesSetting(self, node: ET. Element):
        if node == None:
            raise SettingException.SettingException(21, 'Not Setting for ListModels')

        for child in node:
            try:
                sett = SettingModel.SettingModel()
                sett.SetValuesSetting(child)
                self._ls_setting_models.append(sett)
            except SettingException.SettingException:
                continue

